﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class InputTrackerLocalViewer : MonoBehaviour {

    public Transform m_roomRoot;
    public Transform m_toAffect;
    public XRNode m_vrTracked;

	void Update () {

        m_toAffect.parent = m_roomRoot;
        Vector3 localPos= InputTracking.GetLocalPosition(m_vrTracked);
        Quaternion localRot= InputTracking.GetLocalRotation(m_vrTracked);
        m_toAffect.localPosition = localPos;
        m_toAffect.localRotation = localRot;
    }

    private void Reset()
    {
        m_toAffect = transform;
    }
}
